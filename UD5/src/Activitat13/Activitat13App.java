package Activitat13;
import javax.swing.JOptionPane;

public class Activitat13App {

	public static void main(String[] args) {
		//pido los dos numeros
		String textoNumero1 = JOptionPane.showInputDialog(null, "pon un n�mero");
		int num1 = Integer.parseInt(textoNumero1);
		
		String textoNumero2 = JOptionPane.showInputDialog(null, "pon otro n�mero");
		int num2 = Integer.parseInt(textoNumero2);
		
		String Signo = JOptionPane.showInputDialog(null, "pon que quieres hacer(/,* etc..)");
		//Hago la operacion i la guardo
		double resultado = 0;
		String operacion = "a";
		//Aqu� compruebo que s�mbolo hay dentro de la variable, seg�n el s�mbolo har� una operaci�n u otra
		if (Signo.contentEquals("+")) {
			resultado = num1 + num2;
			operacion = "suma";
		}else if (Signo.contentEquals("-")) {
			resultado = num1 - num2;
			operacion = "resta";
		}else if (Signo.contentEquals("*")) {
			resultado = num1 * num2;
			operacion = "multiplicaci�n";
		}
		else if (Signo.contentEquals("/")) {
			resultado = (double)num1 / (double)num2;
			operacion = "divisi�n";
		}
		else if (Signo.contentEquals("^")) {
			resultado = Math.pow(num1, num2);
			operacion = "elevado";
		}else if (Signo.contentEquals("%")) {
			resultado = num1 % num2;
			operacion = "resto";
		}else {
			JOptionPane.showMessageDialog(null, "El signo no est� bien");
		}
		JOptionPane.showMessageDialog(null, "La " + operacion + " de " + num1 + " y " + num2 + " es igual a: " + resultado);
	}

}
