package Activitat12;
import javax.swing.JOptionPane;

public class Activitat12App {

	public static void main(String[] args) {
		//Creo la contraseņa
		String contraseņa = "Hola";
		//Hago un boolean para hacer el bucle, mientras no ponga bien o se quede sin intentos no sale del bucle
		boolean salida = true;
		int intentos = 3;
		do {
			//Pido la contraseņa la comparo, si es igual ya sale del bucle i le enseņa el mensaje,
			//si no se le dice que la puesto mal i se la pide otra vez
			String intentoContraseņa = JOptionPane.showInputDialog(null, "Pon tu contraseņa, te quedan " + intentos + " intentos");
			if(intentoContraseņa.contentEquals(contraseņa)) {
				JOptionPane.showMessageDialog(null, "Enhorabuena");
				salida = false;
			}else if(intentos == 1) {
				JOptionPane.showMessageDialog(null, "Te has quedado sin intentos");
				salida = false;
			}else {
				JOptionPane.showMessageDialog(null, "Te has equivocado, prueva otra vez");
				intentos--;
			}
		}while(salida);

	}

}
