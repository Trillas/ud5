package Activitat5;
import javax.swing.JOptionPane;

public class Activitat5App {

	public static void main(String[] args) {
		//Pido los datos
		String textoNumero = JOptionPane.showInputDialog(null, "Pon un n�mero");
		double numero = Double.parseDouble(textoNumero);
		//Compruebo su es divisible entre 2
		if ((numero % 2) == 0) {
			JOptionPane.showMessageDialog(null, "Es divisible entre 2");
		}else {
			JOptionPane.showMessageDialog(null, "No es divisible entre 2");
		}
		
	}

}
