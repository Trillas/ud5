package Activitat11;
import javax.swing.JOptionPane;

public class Activitat11App {

	public static void main(String[] args) {
		//Pido el dia de la semana i muestro si es laboral o no
		String dia = JOptionPane.showInputDialog(null, "Pon el dia de la semana");
		switch (dia) {
		case "lunes":
			JOptionPane.showMessageDialog(null, "D�a laboral");
			break;
		case "martes":
			JOptionPane.showMessageDialog(null, "D�a laboral");
			break;
		case "miercoles":
			JOptionPane.showMessageDialog(null, "D�a laboral");
			break;
		case "jueves":
			JOptionPane.showMessageDialog(null, "D�a laboral");
			break;
		case "viernes":
			JOptionPane.showMessageDialog(null, "D�a laboral");
			break;
		case "sabado":
			JOptionPane.showMessageDialog(null, "D�a festivo");
			break;
		case "domingo":
			JOptionPane.showMessageDialog(null, "D�a festivo");
			break;
		default:
			JOptionPane.showMessageDialog(null, "No has introducido un d�a valido");
			break;
		}
	}

}
