package Activitat10;
import javax.swing.JOptionPane;

public class Activitat10App {

	public static void main(String[] args) {
		//Pido cuantas ventas quiere introducir
		String textoNumero = JOptionPane.showInputDialog(null, "Pon el n�mero de ventas");
		double numero = Double.parseDouble(textoNumero);
		//Hago un bucle para pedir todas las ventas i las voy sumando en un total
		double total = 0;
		for (int i = 0; i < numero; i++) {
			String textoVenta = JOptionPane.showInputDialog(null, "Pon la venta");
			double venta = Double.parseDouble(textoVenta);
			total = total + venta;
		}
		//Muestro el total
		JOptionPane.showMessageDialog(null, total);
	}

}
