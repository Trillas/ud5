package Activitat6;
import javax.swing.JOptionPane;

public class Activitat6App {

	public static void main(String[] args) {
		//Pido el precio
		String textoNumero = JOptionPane.showInputDialog(null, "Pon el precio del producto");
		double numero = Double.parseDouble(textoNumero);
		final int IVA = 21;
		//Hago la operación y la muestro
		double precioFinal = ((numero / 100) * IVA) + numero;
		JOptionPane.showMessageDialog(null, precioFinal);
	}

}
