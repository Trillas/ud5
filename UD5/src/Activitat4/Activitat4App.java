package Activitat4;
import javax.swing.JOptionPane;

public class Activitat4App {

	public static void main(String[] args) {
		//Pido los datos
		String textoRadio = JOptionPane.showInputDialog(null, "Pon la �rea del circulo");
		double radio = Double.parseDouble(textoRadio);
		//Hago las operaciones para hacer la �rea i la muestro
		double areaCirculo = (Math.pow(radio, 2)) * Math.PI;
		JOptionPane.showMessageDialog(null, areaCirculo);
	}

}
